import React, { Component } from 'react'
import "./DiagnosticEdit.css";
import axios from 'axios';
import * as Config from '../Config/Config';
import {Link} from "react-router-dom";

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Container from '@material-ui/core/Container';
import { MDBInput } from 'mdbreact';
import Typography from '@material-ui/core/Typography';


//select with multiple choice 
import Checkbox from '@material-ui/core/Checkbox';
// import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
// select
import Select from 'react-select';



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form

//select multiple 
const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;




export default class DiagnosticEdit extends Component {
    constructor(props){  
        super(props); 
        this.state = { 
            changeSymptom:false,
            diseaseName:'',
            description:'',
            errorMessage:'',
            symptoms:[],
            optionsSymptoms:[],
            optionsDoctorSpeciality:[],
            symptomsChecked:[],
            idDoctorSpeciality:'',
            beginningURL: Config.beginningURL
         };
         this.onChangeSymptom=this.onChangeSymptom.bind(this);
         this.onChangeDiseaseName = this.onChangeDiseaseName.bind(this);  
         this.onChangeDescription = this.onChangeDescription.bind(this); 
         this.showErrorMessage=this.showErrorMessage.bind(this);
         this.onSubmit = this.onSubmit.bind(this);
         this.deleteDiagnostic = this.deleteDiagnostic.bind(this);
         this.showOptionSymptoms = this.showOptionSymptoms.bind(this);
       
       
        
    }  
    componentDidMount(){
        // symptoms
        axios.get(this.state.beginningURL+'symptom/byname')
        .then(response=>{
            //all symptoms
            let symptoms=response.data;
            this.setState({symptoms:response.data})
            //symptoms that correspond to the disease 
             let idSymptoms=[]; 

            //id symptoms of the disease (from table symptoms per disease)
              axios.get(this.state.beginningURL+'symptomPerDisease/findbyiddisease/'+this.props.match.params.id)
              .then(response3=>{
                    idSymptoms=response3.data;

                    //set the options of the symptoms
                    let i=0;
                    let choicesSymptoms=[];
                    let toCheck=false;
                    

                    for(i=0;i<symptoms.length;i++){
                        toCheck=false;
                        for(let j=0;j<idSymptoms.length;j++){
                            if(idSymptoms[j].idSymptom===symptoms[i]._id){
                                toCheck=true;
                            }
                        }

                        choicesSymptoms.push({value:symptoms[i]._id, label:symptoms[i].name, toCheck:toCheck});
                    }
                    this.setState({optionsSymptoms:choicesSymptoms});     

                    console.log(choicesSymptoms);            
              })
              .catch(function(error3){
                  console.log(error3);
              });
            
            
        })
        .catch(error=>{
          console.log(error)
        })
       
        //get the disease to update
        axios.get(this.state.beginningURL+'disease/'+this.props.match.params.id)
        .then(response=>{
            this.setState({
                diseaseName:response.data.name,
                description:response.data.description,
                idDoctorSpeciality:response.data.idDoctorSpeciality
            });

            // symptoms:this.state.symptomsChecked

            

            //setting the doctor speciality
             //doctorSpeciality options
            axios.get(this.state.beginningURL+'doctorSpeciality/')
            .then(response=>{
                let options=[];
                let i=0;
                let data=response.data;
                for(i=0;i<data.length; i++){
                    options.push({value:data[i]._id,label:data[i].name});
                    // setting the option so as to match with the current doctor
                    if(data[i]._id===this.state.idDoctorSpeciality){
                        this.setState({ selectedOption:{value:data[i]._id,label:data[i].name} });
                     
                      }

                }
                this.setState({
                    optionsDoctorSpeciality:options
                })

            
            })
            .catch(function(error2){
                console.log(error2);
            })

            //symptoms correspondant to the disease
            

           
        })
        .catch(function(error){
            console.log(error);
        });

        
    }

    onChangeDiseaseName(e){
        this.setState({
            diseaseName:e.target.value
        });
    }
    onChangeDescription(e){
      this.setState({
          description:e.target.value
      });
    }
    
      
    //symptoms multi-select
    onChangeSymptom(e){
        let currentSymptoms  = this.state.symptomsChecked;
        if(e.target.checked===true){
        //   console.log("value="+e.target.value)
        //   console.log("checked="+e.target.checked)
         
          currentSymptoms.push({'id':e.target.value}) ; 
        //   console.log(currentSymptoms)
         this.setState({symptomsChecked:currentSymptoms });
        }
        else{
            // console.log("checked="+e.target.checked);
            currentSymptoms=this.state.symptomsChecked.filter(el=>el.id!==e.target.value)
            // console.log(currentSymptoms);
            this.setState({
                symptomsChecked:currentSymptoms
            });  
        }
     
    }

      //select : DoctorSpeciality
      state = {
        selectedOption: null,
      };
      handleChange = selectedOption => {
        this.setState({ selectedOption });
        //console.log(`Option selected:`, selectedOption);
        this.setState({idDoctorSpeciality:selectedOption.value});
      };

    onSubmit(e){
        e.preventDefault();
     
        //verify if all the required field are not empty
        if((this.state.diseaseName==='' )||(this.state.description==='')||(this.state.idDoctorSpeciality==='')||(this.state.symptomsChecked.length===0)){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
     
                const disease ={
                    name:this.state.diseaseName,
                    description:this.state.description,
                    idDoctorSpeciality:this.state.idDoctorSpeciality,
                    symptoms:this.state.symptomsChecked
                
                };
                console.log(disease);
        
                axios.post(this.state.beginningURL+'disease/updatewithsymptoms/'+this.props.match.params.id,disease)
                 .then(res => console.log(res.data));
        
                window.location="/diagnosticlist";
           
  
          }   
    }
    deleteDiagnostic(){
       
        axios.delete(this.state.beginningURL+'disease/delete/'+this.props.match.params.id)
        .then(res => {
          console.log(res.data);
          window.location="/diagnosticlist";
        });
      
        
    }
  

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
    }
    showOptionSymptoms(){
        if(!this.state.changeSymptom){
            this.setState({changeSymptom:true});
            return(
                <Autocomplete
                            multiple
                            id="checkboxes-tags-demo"
                            options={this.state.optionsSymptoms}
                            disableCloseOnSelect
                            getOptionLabel={(option) => option.label}
                            renderOption={(option, { selected }) => (
                                <React.Fragment>
                                <Checkbox
                                    icon={icon}
                                    checkedIcon={checkedIcon}
                                    style={{ marginRight: 8 }}
                                    checked={option.toCheck}
                                    onChange={this.onChangeSymptom}
                                    onSelect={this.onChangeSymptom}
                                    name={option.label}
                                    value={option.value}
                                />
                                {option.label}
                                </React.Fragment>
                            )}
                            // style={{ width: 500 }}
                            renderInput={(params) => (
                                <TextField  {...params} variant="outlined" label="Symptômes" placeholder="Symptômes"  />
                            )}
                        /> 
            );
        }
        else{
            return(
            <Autocomplete
                            multiple
                            id="checkboxes-tags-demo"
                            options={this.state.optionsSymptoms}
                            disableCloseOnSelect
                            getOptionLabel={(option) => option.label}
                            renderOption={(option, { selected }) => (
                                <React.Fragment>
                                <Checkbox
                                    icon={icon}
                                    checkedIcon={checkedIcon}
                                    style={{ marginRight: 8 }}
                                    checked={selected}
                                    onChange={this.onChangeSymptom}
                                    onSelect={this.onChangeSymptom}
                                    name={option.label}
                                    value={option.value}
                                />
                                {option.label}
                                </React.Fragment>
                            )}
                            // style={{ width: 500 }}
                            renderInput={(params) => (
                                <TextField  {...params} variant="outlined" label="Symptômes" placeholder="Symptômes"  />
                            )}
                        /> 
            );
        }
        
    }

    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
        //select (combobox)
        const { selectedOption } = this.state; 

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        Modification maladie
                                    </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <Button
                                onClick={()=> {this.deleteDiagnostic()}}
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={{marginRight:'5px',marginLeft:'5px', backgroundColor:'red',color:'white'}}                                  
                              >
                                <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>
                                  &nbsp; Supprimer la maladie
                              </Button>
                        
                    <br/>
                    {this.showErrorMessage()}
                    <div className={classesForm.paper} >
                       
                        
                        <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="nommaladie"
                            label="Nom de la maladie"
                            name="nommaladie"
                            autoComplete="nommaladie"
                            autoFocus
                            value={this.state.diseaseName} onChange={this.onChangeDiseaseName}
                        />
                        
                        <label style={{marginTop:'10px'}}>Description</label>
                        <MDBInput type="textarea" placeholder="Description" rows="5" value={this.state.description} onChange={this.onChangeDescription} />
                        
                        <label style={{marginTop:'10px'}}>Docteur Sp&eacute;cialiste</label>
                        <Select
                            placeholder="Docteur Spécialiste"
                            value={selectedOption}
                            onChange={this.handleChange}
                            options={this.state.optionsDoctorSpeciality}
                        />
                        
                        <label style={{marginTop:'10px'}}>Sympt&ocirc;mes</label>
                        {this.showOptionSymptoms()}


                        <Grid container style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                                <Link to="/diagnosticlist"> 
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={{marginRight:'8px'}}                                 
                              >
                                  Annuler
                              </Button>
                              </Link>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={{marginLeft:'8px'}} 
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                        </form>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
