import React, { Component } from 'react'
import "./Index.css";
import Footer from "./../Template/Footer";

export default class Index extends Component {
    render() {
        return (    
            <div className="page-wrapper">  
                {/*-- ============================================================== -->*/}
                {/*-- Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                <div className="page-breadcrumb">
                    <div className="row">
                        <div className="col-12 d-flex no-block align-items-center">
                            <h4 className="page-title">Index</h4>
                            <div className="ml-auto text-right">
                                <nav aria-label="breadcrumb">
                                    <ol className="breadcrumb">
                                        <li className="breadcrumb-item"></li> 
                                        <li className="breadcrumb-item active" aria-current="page">Index</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Bread crumb and right sidebar toggle -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid">
                {/*-- The content of the page-->*/}

                
    
                {/*-- End content of the page-->*/}
                </div>
                {/*-- ============================================================== -->*/}
                {/*-- End Container fluid  -->*/}
                {/*-- ============================================================== -->*/}    
                {/*-- ============================================================== -->*/}
                {/*-- footer -->*/}
                {/*-- ============================================================== -->*/}
                
                <Footer/>

                {/*-- ============================================================== -->*/}
                {/*-- End footer -->*/}
                {/*-- ============================================================== -->*/}
            </div>
        )
    }
}
