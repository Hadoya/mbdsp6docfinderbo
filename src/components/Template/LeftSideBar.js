import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class LeftSideBar extends Component {
    render() {
    return (

        <aside className="left-sidebar" data-sidebarbg="skin5">
            {/* Sidebar scroll*/}
            <div className="scroll-sidebar">
                {/* Sidebar navigation*/}
                <nav className="sidebar-nav">
                    <ul id="sidebarnav" className="p-t-30">
                        <li className="sidebar-item"> <Link to='/dashboard'className="sidebar-link waves-effect waves-dark sidebar-link"  aria-expanded="false"><i className="mdi mdi-view-dashboard"></i><span className="hide-menu">Tableau de bord</span></Link></li>
                        <li className="sidebar-item"> <Link to="/lesaviezvouslist" className="sidebar-link waves-effect waves-dark sidebar-link"  aria-expanded="false"><i className="mdi mdi-view-dashboard"></i><span className="hide-menu">Le Saviez-vous</span></Link></li>
                        <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-blur-linear"></i><span className="hide-menu">Diagnostics & Maladies </span></a>
                            <ul aria-expanded="false" className="collapse  first-level">
                                <li className="sidebar-item"><Link to='/diagnosticlist'className="sidebar-link"><i className="mdi mdi-chart-bubble"></i><span className="hide-menu"> Maladies</span></Link></li>
                                <li className="sidebar-item"><Link to='/symptomlist'className="sidebar-link"><i className="mdi mdi-chart-bubble"></i><span className="hide-menu"> Gestion des sympt&ocirc;mes </span></Link></li>
                            </ul>
                        </li><li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-face"></i><span className="hide-menu">Clients </span></a>
                            <ul aria-expanded="false" className="collapse  first-level">
                                <li className="sidebar-item"><Link to='/clientslist'className="sidebar-link"><i className="mdi mdi-emoticon"></i><span className="hide-menu"> Liste des clients </span></Link></li>
                                <li className="sidebar-item"><Link to='/clientsstatistics'className="sidebar-link"><i className="mdi mdi-chart-bar"></i><span className="hide-menu"> Statistiques </span></Link></li>
                            </ul>
                        </li>
                        <li className="sidebar-item"> <a className="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i className="mdi mdi-face"></i><span className="hide-menu">Corps m&eacute;dicaux </span></a>
                            <ul aria-expanded="false" className="collapse  first-level">
                                <li className="sidebar-item"><Link to='/medicalteamvalidation'className="sidebar-link"><i className="mdi mdi-emoticon"></i><span className="hide-menu"> En attente de validation </span></Link></li>
                                <li className="sidebar-item"><Link to='/medicalteamlist'className="sidebar-link"><i className="mdi mdi-emoticon"></i><span className="hide-menu"> Liste des corps médicaux </span></Link></li>
                                <li className="sidebar-item"><Link to='/medicalteamstatistics'className="sidebar-link"><i className="mdi mdi-chart-bar"></i><span className="hide-menu">Statistiques </span></Link></li>
                            </ul>
                        </li>
                        <li className="sidebar-item"> <Link to='/useradminlist' className="sidebar-link waves-effect waves-dark sidebar-link"  aria-expanded="false"><i className="mdi mdi-emoticon-cool"></i><span className="hide-menu">Administrateurs</span></Link></li>


                        
                    </ul>
                </nav>
                {/* End Sidebar navigation */}
            </div>
            {/* End Sidebar scroll*/}

            {/* ============================================================== */}
        {/* End Left Sidebar - style you can find in sidebar.scss  */}
        {/* ============================================================== */}
        </aside>
        
        
    );
    }
}

