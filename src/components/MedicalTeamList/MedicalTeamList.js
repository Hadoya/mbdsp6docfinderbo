import React, { Component } from 'react'
import "./MedicalTeamList.css";
import axios from 'axios';
import * as Config from './../Config/Config';
//Table
import { Link } from 'react-router-dom';
//import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import {  makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
//grid for the search
import Grid from '@material-ui/core/Grid';
//button
import Button from '@material-ui/core/Button';


const Doctor = props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.doctor.code}>
        <TableCell>{props.numero}</TableCell>
        <TableCell>{props.doctor.lastname+" "+props.doctor.firstname}</TableCell>
        <TableCell>{props.doctor.city+', '+props.doctor.town}</TableCell>
        <TableCell>{props.doctor.description.substring(0,30)}...</TableCell>
        <TableCell style={{textAlign:'center'}}><Link to={"/medicalteamdetails/"+props.doctor._id}><img className="imageModifier" alt="Détails" src="assets/images/logos/logoAdd.png"/></Link></TableCell>  
    </TableRow>
)
  
  const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
  });
//end Table



export default class MedicalTeamList extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
            doctors:[],
            page:0,
            rowsPerPage:10,
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            beginningURL: Config.beginningURL
        }
        //local: http://localhost:5000/
        // en ligne: 
       
        this.doctorList=this.doctorList.bind(this);   
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearchDoctor=this.onSubmitSearchDoctor.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
    
    }  

    componentDidMount(){
        axios.get(this.state.beginningURL+'doctor/okdoctors/')
            .then(response =>{
                //if(response.data.length>0){
                    this.setState({
                        doctors:response.data
                    });
                //}
            })
            .catch(error=>{
                console.log(error);
            })
    }

    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
    }
    onSubmitSearchDoctor(e){
        e.preventDefault();
        const word ={
            wordToSearch:this.state.wordToSearch,
  
        };
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            axios.post(this.state.beginningURL+'doctor/okdoctorssearch',word)
            .then(res => {
                this.setState({
                    doctors:res.data,
                    nbSearchResult:res.data.length,
                    isSubmit:true
                });
            });
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
           return <div><br/><b className="searchResult">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }

    doctorList(){
        let i=0;
        return this.state.doctors.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentDoctor) => {
            i++;
            return <Doctor  numero={i} doctor={currentDoctor} validateDoctor={this.validateDoctor} key={currentDoctor._id}/>;
           
        })
     
    }

    

    render() {
        //for the url
        //const {match} = this.props;
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };

        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table
      

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={6} style={{textAlign:'left'}}>
                                        <h5 style={{marginTop:'10px'}}>Liste des corps m&eacute;dicaux </h5>                                   
                                    </Grid>
                                    <Grid item xs={2}>
                                   
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        <div >
                                        <form onSubmit={this.onSubmitSearchDoctor} noValidate>
                                            <div className="searchDiv">
                                            <InputBase
                                                placeholder=" Rechercher…" 
                                                className="searchInput"  
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}
                                                
                                            />
                                            
                                            <Button variant="contained" type="submit" className="searchButton"><SearchIcon /></Button>
                                            </div>
                                            </form>
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid" style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Paper className={classes.root}>
                    {this.displaySearchResult()}
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                            <TableCell>Num&eacute;ro</TableCell>
                            <TableCell>Nom etPr&eacute;nom(s)</TableCell>
                            <TableCell>Adresse</TableCell>
                            <TableCell>Description</TableCell>
                            <TableCell style={{textAlign:'center'}}>D&eacute;tails</TableCell>
                            </TableRow>
                            
                        </TableHead>
                        <TableBody>
                            {this.doctorList()}
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={this.state.doctors.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                    </Paper>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
