import React, { Component } from 'react'
import "./Login2.css";

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            username:"",
            password:""
        }
    }
    handleSubmit(data) {
        console.log('form submission data', data);
    }

    valid(item,type){
        // to get the value
        //console.warn("value",item.target.value)
        let itemValue = item.target.value;
        //validate the username and the password
        switch(type){
            case "username":{
                //console.warn("item",itemValue,type)
                this.setState({username:itemValue})
            }
            case "password":{
                //console.warn("item",itemValue,type)
                if(item.length<4){
                    item.target.style.color="red";
                }
                else{
                    item.target.style.color="";
                }
                this.setState({password:itemValue})
            }
        }
        //to see the changed state
        console.warn("all", this.state)
    }

    submit(){
        let obj={}
        obj.username=this.state.name;
        obj.password=this.state.password;
        console.warn("submit data", obj)
        this.context.router.history.push(`/template/dashboard`);
    }

    render() {
        return (
            <div className="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div className="auth-box bg-dark border-top border-secondary">
                <div id="loginform">
                    <div className="text-center p-t-20 p-b-20">
                        <span className="db"><img src="assets/images/logo.png" alt="logo" /></span>
                    </div>
                    {/* Form */}
                    <Route path="/" component={Template} />
                    <form className="form-horizontal m-t-20" id="loginform" onSubmit={this.handleSubmit}>
                        <div className="row p-b-30">
                            <div className="col-12">
                                <div className="input-group mb-3">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text bg-success text-white" id="basic-addon1"><i className="ti-user"></i></span>
                                    </div>
                                    <input type="text" className="form-control form-control-lg" placeholder="Nom d'utilisateur" aria-label="Nom d'utilisateur" aria-describedby="basic-addon1" required="" onchange={(item)=>this.valid(item,"username")}/>
                                </div>
                                <div className="input-group mb-3">
                                    <div className="input-group-prepend">
                                        <span className="input-group-text bg-warning text-white" id="basic-addon2"><i className="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" className="form-control form-control-lg" placeholder="Mot de passe" aria-label="Mot de passe" aria-describedby="basic-addon1" required="" onchange={(item)=>this.valid(item,"password")}/>
                                </div>
                            </div>
                        </div>
                        <div className="row border-top border-secondary">
                            <div className="col-12">
                                <div className="form-group">
                                    <div className="p-t-20">
                                        <button className="btn btn-info" id="to-recover" type="button"><i className="fa fa-lock m-r-5"></i> Lost password?</button>
                                        <button className="btn btn-success float-right" type="submit" onClick={()=>this.submit()}>Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="recoverform">
                    <div className="text-center">
                        <span className="text-white">Enter your e-mail address below and we will send you instructions how to recover a password.</span>
                    </div>
                    <div className="row m-t-20">
                        {/* Form */}
                        <form className="col-12" action="index.html">
                            {/* email */}
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text bg-danger text-white" id="basic-addon1"><i className="ti-email"></i></span>
                                </div>
                                <input type="text" className="form-control form-control-lg" placeholder="Email Address" aria-label="Username" aria-describedby="basic-addon1"/>
                            </div>
                            {/* pwd */}
                            <div className="row m-t-20 p-t-20 border-top border-secondary">
                                <div className="col-12">
                                    <a className="btn btn-success" href="#" id="to-login" name="action">Back To Login</a>
                                    <button className="btn btn-info float-right" type="button" name="action">Recover</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        )
    }
}
