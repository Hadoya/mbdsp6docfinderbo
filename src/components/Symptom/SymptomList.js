import React, { Component } from 'react'
import "./SymptomList.css";
import * as Config from './../Config/Config';
//Table
import { Link } from 'react-router-dom';
//import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import {  makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
//grid for the search
import Grid from '@material-ui/core/Grid';
//button
import Button from '@material-ui/core/Button';
//connect mongodb
import axios from 'axios';


const Symptom = props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.symptom.code}>
        <TableCell style={{textAlign:'justify'}}>{props.numero}</TableCell>
        <TableCell style={{textAlign:'justify'}}>{props.symptom.name}</TableCell>
        <TableCell style={{textAlign:'justify'}}>{props.symptom.description}</TableCell>
        <TableCell style={{textAlign:'center'}}><img className="imageSupprimer" alt="supprimer" src="assets/images/logos/LogoDelete.png"  onClick={()=> {props.deleteSymptom(props.symptom._id)}}/></TableCell>
        <TableCell style={{textAlign:'center'}}><Link to={"/editsymptom/"+props.symptom._id}><img className="imageModifier" alt="edit" src="assets/images/logos/editButtonLogo.png"/></Link></TableCell>
    </TableRow>
    )


  const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
  });
//end Table



export default class SymptomList extends Component {
    constructor(props){  
        super(props);  
        this.state = {  
            page:0,
            rowsPerPage:10,
            symptoms:[],
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            beginningURL: Config.beginningURL
        }
      
        this.deleteSymptom=this.deleteSymptom.bind(this); 
        this.symptomList=this.symptomList.bind(this); 
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearchSymptom=this.onSubmitSearchSymptom.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
    } 
         
      
    componentDidMount(){
        axios.get(this.state.beginningURL+'symptom/byname')
        .then(response=>{
          this.setState({symptoms:response.data})
        })
        .catch(error=>{
          console.log(error)
        })
      }

      onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
    }
    onSubmitSearchSymptom(e){
        e.preventDefault();
        const word ={
            wordToSearch:this.state.wordToSearch,
  
        };
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            axios.post(this.state.beginningURL+'symptom/searchfrombo',word)
            .then(res => {
                this.setState({
                    symptoms:res.data,
                    nbSearchResult:res.data.length,
                    isSubmit:true
                });
            });
        }    
    }
    deleteSymptom(id){
        axios.delete(this.state.beginningURL+'symptom/'+id)
        .then(res => console.log(res.data));
        this.setState({
            symptoms:this.state.symptoms.filter(el=>el._id !==id)
        });    
        
    }

    displaySearchResult(){
        if(this.state.isSubmit===true){
           return <div><br/><b className="searchResult">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }

      symptomList(){
        let i=0;
        return this.state.symptoms.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentSymptom) => {
            i++;
            return <Symptom numero={i}  symptom={currentSymptom} deleteSymptom={this.deleteSymptom} key={currentSymptom._id}/>;
           
        })
     
    }

    

    render() {
   
        //for the url
        //const {match} = this.props;
        //Table
        const classes = useStyles;
       
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };

        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table
      

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} style={{textAlign:'left'}}>
                                    <Link to='/newsymptom'>
                                        <Button variant="contained" color="secondary">
                                            Nouveau
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        <div >
                                        <form onSubmit={this.onSubmitSearchSymptom} noValidate>
                                            <div className="searchDiv">
                                            <InputBase
                                                placeholder=" Rechercher…" 
                                                className="searchInput" 
                                                value={this.state.wordToSearch} 
                                                onChange={this.onChangeSearch}
                                               
                                            />
                                            
                                            <Button variant="contained" className="searchButton"><SearchIcon /></Button>
                                            </div>
                                            </form>
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid" style={{marginTop:'20px'}}>
                    
                {/*-- The content of the page-->*/}
                <Paper className={classes.root}>
                    {this.displaySearchResult()}
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table"  >
                        <TableHead>
                          
                            <TableRow>
                                <TableCell>Num&eacute;ro</TableCell>
                                <TableCell>Sympt&ocirc;mes</TableCell>
                                <TableCell>Descriptions</TableCell>
                                {/* <TableCell>Date d'entr&eacute;e</TableCell> */}
                                <TableCell style={{textAlign:'center'}}>Supprimer</TableCell>
                                <TableCell style={{textAlign:'center'}}>Modifier</TableCell>
                            </TableRow>
                        
                            
                            
                        </TableHead>
                        <TableBody>
                            {this.symptomList()}
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={this.state.symptoms.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                    </Paper>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
