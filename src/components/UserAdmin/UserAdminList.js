import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "./UserAdminList.css";
import axios from 'axios';
import * as Config from './../Config/Config';
//import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputBase from '@material-ui/core/InputBase';
import {  makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
//grid for the search
import Grid from '@material-ui/core/Grid';
//button
import Button from '@material-ui/core/Button';


const Administrator = props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.userAdmin.code}>
        <TableCell>{props.numero}</TableCell>
        <TableCell>{props.userAdmin.lastname}</TableCell>
        <TableCell>{props.userAdmin.firstname}</TableCell>
        <TableCell>{props.userAdmin.username}</TableCell>
        <TableCell>{props.userAdmin.email}</TableCell>
        <TableCell style={{textAlign:'center'}}><img className="imageSupprimer" alt="supprimer" src="assets/images/logos/LogoDelete.png" onClick={()=> {props.deleteUserAdmin(props.userAdmin._id)}}/> | <Link to={"/useradminedit/"+props.userAdmin._id}><img className="imageModifier" alt="edit" src="assets/images/logos/editButtonLogo.png"/></Link></TableCell>
        

    </TableRow>
)
  
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
  });

export default class UserAdminList extends Component {
    constructor(props){
        super(props)
        this.state={
            userAdmins:[],
            page:0,
            rowsPerPage:10,
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            beginningURL: Config.beginningURL

        }
        this.deleteUserAdmin=this.deleteUserAdmin.bind(this);
        this.userAdminList=this.userAdminList.bind(this);
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearchUserAdmin=this.onSubmitSearchUserAdmin.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
    }
    componentDidMount(){
        axios.get(this.state.beginningURL+'userAdmin/')
            .then(response =>{
                //if(response.data.length>0){
                    this.setState({
                        userAdmins:response.data
                    });
                //}
            })
            .catch(error=>{
                console.log(error);
            })
    }

    

    deleteUserAdmin(id){
        axios.delete(this.state.beginningURL+'userAdmin/'+id)
        .then(res => console.log(res.data));
        this.setState({
            userAdmins:this.state.userAdmins.filter(el=>el._id !==id)
        });    
        
    }
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
    }
    onSubmitSearchUserAdmin(e){
        e.preventDefault();
        const word ={
            wordToSearch:this.state.wordToSearch,
  
        };
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            axios.post(this.state.beginningURL+'userAdmin/search',word)
            .then(res => {
                this.setState({
                    userAdmins:res.data,
                    nbSearchResult:res.data.length,
                    isSubmit:true
                });
            });
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
           return <div><br/><b className="searchResult">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }

    userAdminList(){
        let i=0;
        return this.state.userAdmins.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentUserAdmin) => {
            i++;
            return <Administrator numero={i} userAdmin={currentUserAdmin} deleteUserAdmin={this.deleteUserAdmin} key={currentUserAdmin._id}/>;
        })
     
    }

    render() {
        //Table
        const classes = useStyles;
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        return (
            <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} style={{textAlign:'left'}}>
                                    <Link to='/useradmincreate'>
                                        <Button variant="contained" color="secondary">
                                            Nouveau
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                    </Grid>
                                    <Grid item xs={4}>

                                        <div >
                                        <form onSubmit={this.onSubmitSearchUserAdmin} noValidate>
                                            <div className="searchDiv">
                                                
                                                <InputBase
                                                placeholder=" Rechercher…" 
                                                value={this.state.wordToSearch}
                                                className="searchInput" 
                                                onChange={this.onChangeSearch}
                                                />
                                            
                                                <Button variant="contained" type="submit" className="searchButton"><SearchIcon /></Button>
                                                
                                            </div>
                                        </form>
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid" style={{marginTop:'20px'}}>

                {/*-- The content of the page-->*/}
                <Paper className={classes.root}>
                    {this.displaySearchResult()}
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Num&eacute;ro</TableCell>
                                <TableCell>Nom</TableCell>
                                <TableCell>Pr&eacute;nom(s)</TableCell>
                                <TableCell>Nom d'utilisateur</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell style={{textAlign:'center'}}>Actions</TableCell>
                            </TableRow>
                            
                        </TableHead>
                        <TableBody>
                            
                                  {this.userAdminList()}
                        
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={this.state.userAdmins.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                    </Paper>  
                {/*-- End content of the page-->*/}
                </div>

               

            </div>
        )
    }
}
