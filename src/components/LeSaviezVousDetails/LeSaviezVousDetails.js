import React, { Component } from 'react'
import "./LeSaviezVousDetails.css";
import axios from 'axios';
import {Link} from "react-router-dom";
import * as Config from './../Config/Config';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {  createMuiTheme,  makeStyles, ThemeProvider  } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { green } from '@material-ui/core/colors';




// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  //theme button 
  const theme = createMuiTheme({
    palette: {
      primary: green,
    },
  });
  //options for select



export default class LeSaviezVousDetails extends Component {
    constructor(props){  
        super(props); 
        //upload photo 
        this.state = {
          idLeSaviezVous:'',
          title:'',
          idCategory:'',
          nomCategory:'',
          image:'',
          description:'',
          beginningURL: Config.beginningURL
         };
    }  
    componentDidMount(){
      axios.get(this.state.beginningURL+'leSaviezVous/'+this.props.match.params.id)
      .then(response=>{
          this.setState({
            idLeSaviezVous:response.data._id,
            title:response.data.title,
            idCategory:response.data.idCategory,
            image:response.data.image,
            description:response.data.description
          })
          axios.get(this.state.beginningURL+'leSaviezVousCategory/'+this.state.idCategory)
          .then(response2=>{
              this.setState({
                nomCategory:response2.data.category,
             
              }) 
          })
          .catch(function(error2){
              console.log(error2);
          });
      })
      .catch(function(error){
          console.log(error);
      });
      
    }
   


    render() {
        //for the url
        //const {match} = this.props;
     
        //form 
        const classesForm = useStylesForm;
    
        

        
        return (
                <div>
                {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
               
                <div className="divAppBar" >
                    <AppBar position="static" style={{textAlign:'right',backgroundColor:'#55acd3'}}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={4} >

                                    </Grid>
                                    <Grid item xs={4} style={{textAlign:'center'}}>
                                    <h5 >
                                        {this.state.title}
                                     </h5>
                                    </Grid>
                                    <Grid item xs={4}>
                                   
                                        
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
               
                {/*-- ============================================================== -->*/}
                {/*-- End Search bar -->*/}
                {/*-- ============================================================== -->*/}
              
                {/*-- ============================================================== -->*/}
                {/*-- Container fluid  -->*/}
                {/*-- ============================================================== -->*/}
                <div className="container-fluid formStyle"  style={{marginTop:'20px'}}>
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    <div className={classesForm.paper} >
                       
                        <div className="contentLeSaviezVousDetails" >
                            <img className="imageLeSaviezVous" src={this.state.image} alt=" Le Saviez-vous"/>
                            
                            <div className="categorieContent row"><h4>Cat&eacute;gorie: {this.state.nomCategory}</h4></div>
                            <div className="descriptionContent row">
                                {this.state.description}
                            </div>
                            <Grid container >
                                <Grid item xs={2} >
                                
                                </Grid>
                                <Grid item xs={5}>
                                  <ThemeProvider theme={theme}>
                                  <Link to='/lesaviezvouslist'>
                                    <Button variant="contained"  > Revenir liste </Button>
                                  </Link>
                                </ThemeProvider>
                                </Grid>
                                <Grid item xs={5}>
                                <ThemeProvider theme={theme}>
                                    <Link to={"/lesaviezvousedit/"+this.state.idLeSaviezVous}>     
                                      <Button variant="contained" color="primary" className={classesForm.submit}>
                                      Modifier/Supprimer
                                      </Button>
                                    </Link>
                                   
                                </ThemeProvider>
                                </Grid>
                            </Grid>
                        </div>
                        

                        
                        
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
             </div>
        )
    }
}
