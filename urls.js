module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://docfinder-admin.herokuapp.com',
    qa:'https://docfinder-admin-qa.herokuapp.com'
}